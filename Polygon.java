package day5;

import java.util.Scanner;

public class Polygon {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();  //t:2

        while (t-- > 0) {
            int n = sc.nextInt();      //n:3             4
            int s[] = new int[n];
            int max = s[0];
            int sum = s[0];
            for (int i = 0; i < n; i++) {
                s[i] = sc.nextInt();   // 4  3  2              1  2    1   4
                if (max < s[i])
                    max = s[i];//max:4      max:1
                sum = sum + s[i];
            }
            sum = sum - max;    //sum:9   max:4         || sum:8   max:4
            if (sum > max)
                System.out.println("Yes");
            else
                System.out.println("No");
        }

    }
}
