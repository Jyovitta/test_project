package day7;

import java.util.Arrays;
import java.util.Scanner;

public class HelpJarvis {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String arr[] = new String[n];

        for (int i = 0; i < n; i++) {
            arr[i] = sc.next();

            char arr1[] = arr[i].toCharArray();
            Arrays.sort(arr1);
            int s = 0;
            boolean flag = false;
            for (int j = 0; j < arr1.length - 1; j++) {
                s = arr1[j + 1] - arr1[j];
                if (s > 1) {
                    flag = false;
                    break;
                } else {
                    flag = true;
                }
            }
            if (flag) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }
    }
}
