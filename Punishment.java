package day8;

import java.util.ArrayList;
import java.util.Scanner;

public class Punishment {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        ArrayList<Long> arr= new ArrayList<Long>();
        int t1=1, t2=1;
        for(int i=1;i<=n;i++) {
            if(i%2!=0) {
                arr.add((long)(Math.pow(2,t1))*10);
            t1++;  //4
            }
            if(i%2==0) {
                arr.add((long)(Math.pow(3,t2))*10);
                t2++;  //3
            }//{20,30,40}
        }
        long max=arr.get(0);
        for(int i=0;i<n;i++){
            if(max<arr.get(i))  //max:20
                max=arr.get(i);
        }
        System.out.println(2*max);
    }
}
