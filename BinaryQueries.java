package day5;

import java.util.Scanner;

public class BinaryQueries {
    public static void main(String args[]) throws Exception {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();//5
        int q = sc.nextInt();//2
        int a[] = new int[n];
        for (int i = 0; i < n; i++) { //n:1
            a[i] = sc.nextInt();  //[10110]
        }
        for (int i = 0; i < q; i++) {     //q:2
            int t = sc.nextInt();    //1  0
            if (t == 0) {
                int l = sc.nextInt(); //1
                int r = sc.nextInt(); //4
                if (a[r - 1] == 1) {
                    System.out.println("ODD");
                } else {
                    System.out.println("EVEN");
                }
            } else {
                int j = sc.nextInt();//j:2
                if (a[j - 1] == 1) {
                    a[j - 1] = 0;
                } else {
                    a[j - 1] = 1;
                }
            }
        }

    }

}
