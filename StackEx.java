package day7;

import java.util.Scanner;
import java.util.Stack;

public class StackEx {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        s.nextLine();
        String str = s.nextLine();
        Stack<Character> stack = new Stack<>();
        for (int i =0;i<n;i++){
            if(!stack.empty() && stack.peek()==str.charAt(i)){
                stack.pop();
            } else
                stack.push(str.charAt(i));
        }
        String result = "";
        System.out.println(stack.size());
        while(!stack.empty()){
            result = stack.peek() + result;
            stack.pop();
        }
        System.out.println(result);


    }
}
