package day9;

import java.util.Arrays;
import java.util.Scanner;

public class PrintUnique {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        int n=sc.nextInt();
        while (t-->0){
            int a[]=new int[n];
            for (int i=0;i<n;i++){
                a[i]=sc.nextInt();
            }
            Arrays.sort(a);
            int count=1;
            for (int i=1;i<n;i++) {
                if (a[i] == a[i - 1]) {
                    count = count + 1;
                } else {
                    if (count == 3) {
                        count = 1;
                    } else {
                        System.out.println(a[i - 1]);
                        count = 1;
                    }
                }
            }
            System.out.println(a[n-1]);
        }
    }
}
