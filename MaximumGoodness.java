package day7;

import java.util.Scanner;

public class MaximumGoodness {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();    //14
        int arr[]=new int[n];
        for(int i=0;i<n;i++){
            arr[i]=sc.nextInt();   //1 1 1 1 0 0 1 1 0 0 0 1 0 0
            if(arr[i]==0){
                arr[i]=-1;
            }
        }
        int max=0;
        int current=0;
        int start=0;
        int end=0;
        int t=0;

        for(int i=0;i<n;i++) {
            current+= arr[i];
            if (current >= max) {
                max=current;
                end = i;
            }
            if (current < 0) {
                current = 0;
                start = end + 1;

            }
            if (end - start >= t) {
                t = end - start + 1;
            }
        }
        System.out.println(t);
    }
}
