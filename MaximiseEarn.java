package day6;

import java.util.Scanner;

public class MaximiseEarn {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();

        while (t-- > 0) {
            int n = sc.nextInt();
            int k = sc.nextInt();

            int s[] = new int[n];

            for (int i = 0; i < n; i++) {
                s[i] = sc.nextInt();

            }

            int sum = 1;
            int max = 0;
            for (int j = 0; j < n; j++) {

                if (s[j] > s[max]) {

                    max = j;       //max:3
                    sum++;
                }
            }

            System.out.println(sum * k);

        }
    }
}
