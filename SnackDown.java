package day8;

import java.util.Scanner;

public class SnackDown {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        int n = sc.nextInt();
        int n1 = sc.nextInt();
        int n2 = sc.nextInt();
        int a[] = new int[n1];
        int b[] = new int[n2];
        while (t-- > 0) {
            for (int i = 0; i < n1; i++) {
                a[i] = sc.nextInt();
            }
            for (int i = 0; i < n2; i++) {
                b[i] = sc.nextInt();
            }
            int c[] = new int[n1 + n2];
            int count = 0;
            for (int i = 0; i < n1; i++) {
                c[count] = a[i];
                count += 1;
            }
            for (int i = 0; i < n2; i++) {
                c[count] = b[i];
                count += 1;
            }
          //  boolean check1 = true;
            boolean check = false;
            for (int i = 1; i <= n; i++) {
                check = false;
                for (int j = 0; j < count; j++) {   //2  3  2  2  count:4
                    if (i == c[j]) {
                        check = true;
                        break;
                    }
                }

                /*if (check) {
                    check1 = true;
                } else {
                    break;
                }*/
            }
            if (check==false) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }
    }
}
