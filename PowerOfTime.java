package day6;

import java.util.Scanner;

public class PowerOfTime {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n=sc.nextInt();   //n:3
        int a[]=new int[n];
        int b[]=new int[n];
        for(int i=0;i<n;i++)
            a[i]=sc.nextInt();   //3  2  1
        for(int i=0;i<n;i++)
            b[i]=sc.nextInt();    //1  3  2
        int i=0,count=0;
        while(i<n)
        {
            if(a[i]==b[i])
            {
                i++;
                count++;
            }
            else
            {
                int temp=0;
                for(int j=i;j<n-1;j++)
                {
                    temp=a[j];
                    a[j]=a[j+1];
                    a[j+1]=temp;     //{2  1  3}    {1  3  2}    {3  2  1}
                }
                count++;
            }
        }
        System.out.println(count);
    }
}
