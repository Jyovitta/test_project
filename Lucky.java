package day7;

import java.util.Scanner;

public class Lucky {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t=sc.nextInt();//2
        int arr[];
        while(t-- >0){
            int n=sc.nextInt();//5
            arr=new int[n];
            for(int i=0;i<n;i++)
                arr[i]=sc.nextInt();// 5 5 6 4 3 3 3
            int min=arr[0];
            int l=1;
            for(int i=1;i<n;i++){
                if(min==arr[i])
                    l++;   //2
                if(min>arr[i]){
                    min=arr[i];
                    l=1;
                }
            }
            if(l%2!=0)
                System.out.println("Lucky");
            else
                System.out.println("Unlucky");
        }

    }
}
