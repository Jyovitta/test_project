package day6;

import java.util.Scanner;

public class TakeOff {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while(t-->0){
            int n=sc.nextInt();    //10
            int p=sc.nextInt();    //2
            int q=sc.nextInt();    //3
            int r=sc.nextInt();    //4
            int total=0;
            for(int i=p;i<=n;i+=p){
                if(i%q!=0 && i%r!=0){
                    total++;
                }
            }
            for(int i=q;i<=n;i+=q){
                if(i%p!=0 && i%r!=0){
                    total++;
                }
            }
            for(int i=r;i<=n;i+=r){
                if(i%p!=0 && i%q!=0){
                    total++;
                }
            }
            System.out.println(total);
        }
    }
}
