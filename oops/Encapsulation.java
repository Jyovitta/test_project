package com.company.day1and2and3;

public class Encapsulation {
    public static void main(String[] args) {
        Truck truck=new Truck();
        truck.setMake("ford");
        truck.setModel("f3556");
        truck.setYear(2016);
        truck.setTonnage(5.489);
        System.out.println("TRUCK DETAILS");
        System.out.println("MAKE  "+truck.getMake());
        System.out.println("MODEL  "+truck.getModel());
        System.out.println("YEAR  "+truck.getYear());
        System.out.println("TONNAGE  "+truck.getTonnage());
        System.out.println();
       // truck.print();
        Car car=new Car();
        car.setMake("hbuj");
        car.setModel("y45677");
        car.setYear(2020);
        car.setColor("pink");
        System.out.println("CAR DETAILS");
        System.out.println("MAKE  "+car.getMake());
        System.out.println("MODEL  "+car.getModel());
        System.out.println("YEAR  "+car.getYear());
        System.out.println("COLOR  "+car.getColor());
       // car.print();
    }
}
