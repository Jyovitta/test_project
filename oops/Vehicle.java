package com.company.day1and2and3;

public class Vehicle {
     private String make;
     private String model;
     private int year;

    public Vehicle(String make, String model, int year) {
        this.make = make;
        this.model = model;
        this.year = year;
    }

    public Vehicle() {
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setYear(int year) {
        this.year = year;
    }

    void print()
    {
        System.out.println("Make: " + make + ", Model: " + model + ", Year: " + year);
    }
}
