package com.company.day1and2and3;

public class Car extends Vehicle {
    String color;

    public Car(String make, String model, int year, String color) {
        super(make, model, year);
        this.color = color;
    }

    public Car() {
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void print(){
        System.out.println("CAR DETAILS");
        super.print();
        System.out.print("color: " + color);
    }
}
