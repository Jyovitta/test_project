package com.company.day1and2and3;

public class Inheritance {
    public static void main(String[] args) {
        Truck truck=new Truck("ford","f28390",2018,5.356);
        Car car=new Car("swift","sw5637",2019,"Blue");
        truck.print();
        car.print();
    }
}
