package com.company.day1and2and3;

public class Truck extends Vehicle {
    double tonnage;

    public Truck(String make, String model, int year, double tonnage) {
        super(make, model, year);
        this.tonnage = tonnage;
    }

    public Truck() {

    }

    /*public Truck(double tonnage) {
        this.tonnage = tonnage;
    }*/

    public double getTonnage() {
        return tonnage;
    }

    public void setTonnage(double tonnage1) {
        tonnage = tonnage1;
    }

    void print()
    {
        System.out.println("TRUCK DETAILS");
        super.print();
        System.out.println("Tonnage: " + tonnage);
    }
}
