package com.company.day1and2and3;

public class Polymorphism {
    public static void main(String[] args) {
        Vehicle vehicle;
        vehicle =new Truck("ford","f56378",2018,5.748);
        vehicle.print();
        vehicle=new Car("swift","sw6748",2020,"purple");
        vehicle.print();

    }
}
