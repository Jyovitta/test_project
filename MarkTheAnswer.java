package day7;

import java.util.Scanner;

public class MarkTheAnswer {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
         int n=sc.nextInt();  //7
         int k=sc.nextInt();   //6
         int skipped=0;
         int arr[]=new int[n];
         for (int i=0;i<n;i++){
             arr[i]=sc.nextInt();  //4 3 7 6 7 2 2
         }
         int count = 0;
         for (int i=0;i<n;i++){
             if (arr[i]<=k){
                 count++;   //3
             }else if (arr[i]>k && skipped==0){
                 skipped=1;
             }else {
                 break;
             }
         }
        System.out.println(count);
    }
}
