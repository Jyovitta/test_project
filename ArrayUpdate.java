package day5;

import java.util.Scanner;

public class ArrayUpdate {
    public static void main(String[] args) {

    Scanner s = new Scanner(System.in);
    int t= s.nextInt();  //t=2
        while(t-->0) {
            int n = s.nextInt();  //3
            int k = s.nextInt();//4
            int min = Integer.MAX_VALUE;  //2147483647
            for (int i = 0; i < n; i++) {    //0  1  2
                int temp = s.nextInt();//1  2  5
                if (temp < min)
                    min = temp;//min = 1
            }
            if (min >= k) {
                System.out.println(0);
            } else{
                System.out.println(k - min);//2  5  5
                }
        }
    }
}
