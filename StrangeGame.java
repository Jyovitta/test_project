package day6;


import java.io.IOException;
import java.util.Scanner;

public class StrangeGame {
    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            int s[] = new int[sc.nextInt()];
            int n = sc.nextInt();
            int k = sc.nextInt();
            int s1[] = new int[n];
            int s2[] = new int[n];
            int max = Integer.MIN_VALUE;
            // int max=s1[0];
            int total = 0;
            for (int i = 0; i < n; i++) {
                s1[i] = sc.nextInt();
                if (s1[i] > max) {
                    s1[i] = max;
                }
                max++;
            }
            //max++;
            for (int i = 0; i < n; i++) {
                s2[i] = sc.nextInt();
                if (max - s2[i] > 0) {
                    total += max - s2[i] * k;
                }
            }
            System.out.println(total);
        }
    }

}
